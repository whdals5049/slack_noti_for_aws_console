variable "environment" {
  type = string
  description = "deployment environment"
}


# output "account_id" {
#   value = "${data.aws_caller_identity.current.account_id}"
# }

variable "lambda_arn" {
  description = "The ARN of the Lambda function to trigger"
  type        = string
  # default = "arn:aws:lambda:ap-northeast-2:${data.aws_caller_identity.current.account_id}:function:jmkim-slackAlarm-lambda-function"
}