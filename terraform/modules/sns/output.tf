output "aws_sns_topic"{
  value = aws_sns_topic.sns.id
}

output "aws_sns_topic_subscription"{
  value = aws_sns_topic_subscription.lambda.topic_arn
}

output "aws_sns_topic_subscription_lambda_endpoint"{
  value = aws_sns_topic_subscription.lambda.endpoint
}

output "aws_sns_policy"{
  value = aws_sns_topic_policy.sns.id
}