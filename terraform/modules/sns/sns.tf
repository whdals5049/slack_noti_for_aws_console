# modules/sns/main.tf

# Create the SNS topic
resource "aws_sns_topic" "sns" {
  name = "${var.environment}-jmkim-slackAlarm-sns-topic"
}

# Create a subscription to the SNS topic that triggers the Lambda function
resource "aws_sns_topic_subscription" "lambda" {
  topic_arn = aws_sns_topic.sns.arn
  protocol  = "lambda"
  endpoint  = var.lambda_arn
}

# Create a policy that allows the event bridge rule to publish to the SNS topic
data "aws_iam_policy_document" "sns" {
  policy_id = "__default_policy_ID"
  version = "2008-10-17"

  statement {
    actions = [
      "SNS:GetTopicAttributes",
      "SNS:SetTopicAttributes",
      "SNS:AddPermission",
      "SNS:RemovePermission",
      "SNS:DeleteTopic",
      "SNS:Subscribe",
      "SNS:ListSubscriptionsByTopic",
      "SNS:Publish"
    ]

    condition {
      test     = "StringEquals"
      variable = "AWS:SourceOwner"

      values = [
        "932580681803" ## variable
      ]
    }

    effect = "Allow"

    principals {
      type        = "AWS"
      identifiers = ["*"]
    }

    resources = [aws_sns_topic.sns.arn]

    sid = "__default_statement_ID"
  }

  statement {
    # sid = "${aws_cloudwatch_event_rule.slack_eventbridge_pattern.name}-${aws_cloudwatch_event_rule.slack_eventbridge_pattern.id}"

    effect = "Allow" 
    actions = ["SNS:Publish"] 

    principals {
      type        = "Service"
      identifiers = ["events.amazonaws.com"]
    }

    resources = [aws_sns_topic.sns.arn]
  }
}

resource "aws_sns_topic_policy" "sns" {
  arn    = aws_sns_topic.sns.arn
  policy = data.aws_iam_policy_document.sns.json
}
