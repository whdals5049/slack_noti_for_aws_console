variable "environment" {
  type = string
  description = "deployment environment"
}

variable "target_topic_arn" {
  description = "The ARN of the SNS topic to send events to"
  type        = string
}