output "aws_cloudwatch_event_rule"{
  value = aws_cloudwatch_event_rule.eventbridge.id
}

output "aws_cloudwatch_event_bus"{
  value = aws_cloudwatch_event_rule.eventbridge.event_bus_name
}


output "aws_cloudwatch_event_target_1"{
  value = aws_cloudwatch_event_target.default_eventbridge.id
}

# output "aws_cloudwatch_event_target_us_east_1"{
#   value = aws_cloudwatch_event_target.us_east_1_eventbridge.id
# }