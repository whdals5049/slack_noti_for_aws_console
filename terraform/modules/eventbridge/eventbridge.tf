# modules/eventbridge/main.tf

# Create the event bridge rule
resource "aws_cloudwatch_event_rule" "eventbridge" {
  name                = "${var.environment}-jmkim-slackAlarm-eventbridge-rule"
  event_pattern = jsonencode({
  "source": ["aws.iam", "aws.ec2", "aws.signin", "aws.lambda", "aws.dynamodb", "aws.s3", "aws.ecr", "aws.eks"]
  "detail": {
    "eventName": ["AttachGroupPolicy", "AttachRolePolicy", "AttachUserPolicy", "ChangePassword", "CreateAccessKey", "CreateGroup", "CreatePolicy", "CreateRole", "CreateUser", "DeleteAccessKey", "DeleteGroup", "DeleteGroupPolicy", "DeletePolicy", "DeleteRole", "DeleteRolePolicy", "DeleteUser", "DeleteUserPolicy", "DetachGroupPolicy", "DetachRolePolicy", "DetachUserPolicy", "PutGroupPolicy", "PutRolePolicy", "PutUserPolicy", "AuthorizeSecurityGroupIngress", "AuthorizeSecurityGroupEgress", "RevokeSecurityGroupIngress", "RevokeSecurityGroupEgress", "StopInstances", "TerminateInstances", "RunInstances", "StartInstances", 
    "ConsoleLogin", "GetFunction20150331v2", "GetPolicy20150331v2", "UpdateFunctionCode20150331v2", "GetFunctionCodeSigningConfig", 
    "DeleteTable", "CreateTable", "AltarTable", "Scan", "GetItems", "DeleteBucket", "CreateBucket", "GetBucketAcl", "PutBucketAcl", "GetBucketPolicy", "PutBucketPolicy", "GetBucketOwnershipControls",
     "PutImage", "BatchDeleteImage", "DeleteCluster", "DeleteNodegroup"],
    "eventSource": ["iam.amazonaws.com", "ec2.amazonaws.com", "signin.amazonaws.com", "lambda.amazonaws.com", "dynamodb.amazonaws.com", "s3.amazonaws.com", "ecr.amazonaws.com", "eks.amazonaws.com"]
  },
  })
  event_bus_name      = "default"
}

# Add a target to the event bridge rule that sends events to the SNS topic
resource "aws_cloudwatch_event_target" "default_eventbridge" {
  rule      = aws_cloudwatch_event_rule.eventbridge.name
  arn       = var.target_topic_arn
  target_id = "${var.environment}-jmkim-slackAlarm-sns-target"
  depends_on = [aws_cloudwatch_event_rule.eventbridge]
}


# resource "aws_cloudwatch_event_target" "us_east_1_eventbridge" {
#   rule      = aws_cloudwatch_event_rule.eventbridge.name
#   arn       = var.target_topic_arn
#   target_id = "${var.environment}-jmkim-slackAlarm-sns-target"
# }
/*  */
resource "aws_cloudwatch_event_target" "us_east_1_eventbridge" {
  rule      = aws_cloudwatch_event_rule.eventbridge.name
  # "arn:aws:sns:ap-northeast-2:932580681803:prod-jmkim-slackAlarm-sns-topic"
  arn       = "arn:aws:events:us-east-1:932580681803:event-bus/default"
  target_id = "CG-GOV-EVENT-BRIDGE-RULE"
  role_arn  = "arn:aws:iam::932580681803:role/service-role/prod-slackAlarm-Amazon_EventBridge_Invoke_Event_Bus_1642352426"
  depends_on = [aws_cloudwatch_event_rule.eventbridge]
}
