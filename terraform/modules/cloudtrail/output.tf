output "cloudtrail_name_s3"{
  value = aws_cloudtrail.foobar.id
}


output "s3_store_cloudtrail_log"{
  value = aws_s3_bucket.foo.id
}

output "account_id" {
  value = data.aws_caller_identity.current.account_id
}