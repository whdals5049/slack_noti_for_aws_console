

const AWS = require('aws-sdk');
AWS.config.update({
  region: 'ap-northeast-2',
  endpoint: "https://dynamodb.ap-northeast-2.amazonaws.com"
});

// DynamoDB 선언
// const ddb = new AWS.DynamoDB({apiVersion: '2012-08-10'});
const docClient = new AWS.DynamoDB.DocumentClient({apiVersion: '2012-08-10'}); 


console.log("DynamoDB start     ");

let item = {};
function objectSeperation(obj){
  for(let element in obj){
    // console.log(`typeof ${element}:    ${typeof obj[element]} `);
    if (typeof obj[element] != 'object' ){
      item[`${element}`] = obj[element];
    } else if(obj[element] == '' || obj[element] == {} || obj[element] == []) {
      // console.log(element);
      item[`${element}`] = `EMPTY`;
      
    } else {
      // console.log(element);
      item[`${element}`] = `[Object] LENGTH: ${Object.keys(obj[element]).length} `;
      objectSeperation(obj[element]);
    }
    
  }


  return item;

}



let ITEMS = {
  version: '0',
  id: '2dfb0948-0b48-c393-c5ab-a94cbc84eb2f',
  'detail-type': 'AWS API Call via CloudTrail',
  source: 'aws.ec2',
  account: '932580681803',
  time: '2023-05-03T02:25:10Z',
  region: 'ap-northeast-2',
  resources: [],
  detail: {
      eventVersion: "1.08",
      userIdentity: {
          type: "IAMUser",
          principalId: "AIDA5SIRD5BFXY35T3RKI",
          arn: "arn:aws:iam::932580681803:user/jmkim55@sk.com",
          accountId: "932580681803",
          accessKeyId: "ASIA5SIRD5BFWQJL7QU6",
          userName: "jmkim55@sk.com",
          sessionContext: {
              sessionIssuer: {},
              webIdFederationData: {},
              attributes: {
                  creationDate: "2023-05-03T00:08:52Z",
                  mfaAuthenticated: "false"
              }
          }
      },
      eventTime: "2023-05-03T01:24:51Z",
      eventSource: "ec2.amazonaws.com",
      eventName: "StartInstances",
      awsRegion: "ap-northeast-2",
      sourceIPAddress: "61.33.137.194",
      userAgent: "AWS Internal",
      requestParameters: {
          instancesSet: {
              items: [
                  {
                      instanceId: "i-072c87d4dc8715610"
                  }
              ]
          }
      },
      responseElements: {
          requestId: "f2b168d8-8fd5-4973-99c7-8c5675df89a9",
          instancesSet: {
              items: [
                  {
                      instanceId: "i-072c87d4dc8715610",
                      currentState: {
                          code: 0,
                          name: "pending"
                      },
                      previousState: {
                          code: 80,
                          name: "stopped"
                      }
                  }
              ]
          }
      },
      requestID: "f2b168d8-8fd5-4973-99c7-8c5675df89a9",
      eventID: "1f169f34-d171-4952-a5a4-465bc3e67b5e",
      readOnly: false,
      eventType: "AwsApiCall",
      managementEvent: true,
      recipientAccountId: "932580681803",
      eventCategory: "Management",
      sessionCredentialFromConsole: "true"
  }
}
ITEMS = objectSeperation(ITEMS);
console.log(typeof ITEMS);


// console.log("KEYS: ", Object.keys(ITEMS));

// for (let key in ITEMS) {
  // console.log('type key: ', typeof key);
  // let letter = '\'' + key + '\'';
  // ITEMS[letter] = ITEMS[key];
  
  // 기존 프로퍼티 제거
  // delete ITEMS[key];
// }




console.log("ITEMS::  ", ITEMS);

var params = {
  TableName: 'test-slackalarm-db-ap-northeast-2',
  Item: 
  ITEMS
    // {

      // 'userName': ITEMS.userName,
      // 'eventTime': ITEMS.eventTime,
      // 'detailState': ITEMS.detailState,
      // 'accountMessage': ITEMS.account,
      // 'eventName': ITEMS.eventName,
      // 'detailTypeMessage': ITEMS['detail-type'],
      // "sourceIP": ITEMS.sourceIPAddress,
      // "eventID": ITEMS.eventID,
      // 'awsRegion': ITEMS.awsRegion
    // }
    
  }
;

console.log('eventName:  ', ITEMS['eventName']);

docClient.put(params, function(err, data) {
  if (err) {
    console.log("Put Error", err);
  } else {
    console.log("Put Item Success", data);
  }
});
console.log("DynamoDB Put Items Done   ");


