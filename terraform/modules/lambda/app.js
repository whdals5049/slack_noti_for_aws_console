// slack attachments:  https://api.slack.com/messaging/composing/layouts
// alarm nodeJS: https://velog.io/@wngud4950/AWS-Lambda%EB%A1%9C-Slack-%EC%9E%90%EB%8F%99%EC%95%8C%EB%A6%BC-%EC%83%9D%EC%84%B1%ED%95%98%EA%B8%B0
// Emoji Link   https://itinerant.tistory.com/60
// const fs = require('fs');
const AWS = require('aws-sdk');
AWS.config.update({
  region: 'ap-northeast-2',
  endpoint: "https://dynamodb.ap-northeast-2.amazonaws.com"
});

// // S3 선언
// const s3 = new AWS.S3({
//   sslEnabled: false,
// });


// DynamoDB 선언
// const ddb = new AWS.DynamoDB({apiVersion: '2012-08-10'});
const docClient = new AWS.DynamoDB.DocumentClient({apiVersion: '2012-08-10'}); 


// Slack alarm 설정
const https = require('https');

const SLACK_URL = process.env.HOOK_URL;
const CHANNEL = process.env.CHANNEL;

const BLACK = "#000000";
const WHITE = "#ffffff";
const RED = "#ff000f";
const GREEN = "#00ff0f"
const BLUE = "#0000ff";
const ORANGE = "#ff8000";
const YELLOW = "#ff4000";
const CYAN = "#00ffff";
const MAGENTA = "#ff00ff"
let COLOR = WHITE;

let FALLBACK = "...";
let EMOJI_SET = [":smile:", ":blush:", ":information_source:", ":sos:", ":interrobang:" ,":cloud:", ":warning:", ":rotating_light:"];
let EMOJI = EMOJI_SET[0];
const EVENT = ["iam.amazonaws.com", "ec2.amazonaws.com", "signin.amazonaws.com", "lambda.amazonaws.com", "s3.amazonaws.com", "dynamodb.amazonaws.com"];

let item = {};
function objectSeperation(obj){
  for(let element in obj){
    // console.log(`typeof ${element}:    ${typeof obj[element]} `);
    if (typeof obj[element] != 'object' ){
      item[`${element}`] = obj[element];
    } else if(obj[element] == '' || obj[element] == ' ' || obj[element] == {}) {
      item[`${element}`] = `EMPTY`;
      
    } else if(obj[element] == undefined) {
      item[`${element}`] = `undefined`;
      
    } else if( obj[element] == null) {
      item[`${element}`] = `null`;
      
    } else {
      item[`${element}`] = `[Object] LENGTH: ${Object.keys(obj[element]).length}`;
      objectSeperation(obj[element]);
    }
    
  }


  return item;

}

exports.handler = async (event, context, callback) => {
  context.callbackWaitsForEmptyEventLoop = false;
  
  console.log('event:', event);
  // console.log('context: ', context);
  
  const snsEvent = event['Records'][0]['Sns'];
  console.log('snsEvent:  ', snsEvent);
  console.log('typeof snsEvent');
  

  ///////////////////////////////////////
  const receivedEvent = JSON.parse(snsEvent['Message']);
  console.log('type receivedEvent:    ', typeof receivedEvent);
  console.log('receivedEvent(Message) :  ', receivedEvent);

  
  console.log('detail::  ', receivedEvent['detail']);
  const detailEvent = receivedEvent['detail'];
  
  const userType = detailEvent['userIdentity']['type'];
  const userName = detailEvent['userIdentity']['userName'];
  const eventTime = detailEvent['eventTime'];
  const eventName = detailEvent['eventName'];
  const eventSource = detailEvent['eventSource'];
  
  const sourceIP = detailEvent['sourceIPAddress'];
  const awsRegion = detailEvent['awsRegion'];
  const eventID = detailEvent['eventID'];
  const eventType = detailEvent['eventType'];
  
  const cloudTrailUrl = `https://${awsRegion}.console.aws.amazon.com/cloudtrail/home?region=${awsRegion}#/events/${eventID}`;
  
  

  if (userType == "Root"){
    COLOR = BLACK;
    EMOJI = EMOJI_SET[4];
  } else {
    if (eventSource.search("iam") >= 0){
      if (eventName.search("Detach") >= 0 || eventName.search("Delete") >= 0 || eventName.search("Revoke") >= 0){
        COLOR = RED;
      } else {
        COLOR = GREEN;
      }
      FALLBACK = "IAM 변경 감지";      
    } else if(eventSource.search("ec2") >= 0){
        if (eventName.search("Stop") >= 0 || eventName.search("Terminate") >= 0 || eventName.search("Revoke") >= 0){
          COLOR = RED;
        } else {
          COLOR = GREEN;
        }
        FALLBACK = "EC2 변경 감지";
    } else if(eventSource.search("Lambda") >= 0){
      if (eventName.search("Update") >= 0 || eventName.search("Get") >= 0 ){
        COLOR = RED;
      } else {
        COLOR = ORANGE;
      }
      FALLBACK = "Lambda 변경 감지"
    } else if(eventSource.search("s3") >= 0){
      if (eventName.search("Put") >= 0 || eventName.search("Get") >= 0 || eventName.search("Delete") >= 0){
        COLOR = YELLOW;
        EMOJI = EMOJI_SET[3];
      } else {
        COLOR = GREEN;
      }    
      FALLBACK = "S3 변경 감지"
    } else if(eventSource.search("dynamodb") >= 0){
      if (eventName.search("Altar") >= 0 || eventName.search("Delete") >= 0){
        COLOR = RED;
      } else {
        COLOR = CYAN;
      }
      FALLBACK = "DynamoDB 변경 감지"
    } else if(eventSource.search("signin") >= 0){
      COLOR = ORANGE;
      FALLBACK = "Console Login 감지";
      if (userName == "whdals5049@naver.com"){
        EMOJI = EMOJI_SET[1];
      }
    } else{
      COLOR = MAGENTA;
    }
  } 
  
  let slackWebhookPayload = payloadTemplate(userType, userName, awsRegion, eventTime, eventName, eventSource, sourceIP, cloudTrailUrl);

  await postSlack(slackWebhookPayload, SLACK_URL);


  ////////////////////////////////////// Dynamodb

  console.log("DynamoDB start     ");

  // let time = new Date(eventTime);
  // const TIME = `${time.getFullYear()}-${dateFormat(time.getMonth()+1)}-${dateFormat(time.getDate())} ${dateFormat(time.getHours() + 9)}:${dateFormat(time.getMinutes())}:${dateFormat(time.getSeconds())} GMT+9`
  let ITEMS = objectSeperation(receivedEvent);

  console.log("ITEMS::  ", ITEMS);
  console.log("TYPEOF ITEMS::  ", typeof ITEMS);

  var params = {
    TableName: 'test-slackalarm-db-ap-northeast-2',
    Item: 
      ITEMS
      // 'userName': userName,
      // 'timeMessage': TIME,
      // 'messageID': messageID,
      // 'detailState': detailState,
      // 'accountMessage': accountMessage,
      // 'detailTypeMessage': detailTypeMessage,
      // "sourceIP": sourceIP,
      // "eventID": eventID,
      // 'awsRegion': awsRegion
      
  
  };

  console.log('eventName:  ', ITEMS['eventName']);
  console.log("DynamoDB Put Items Done   ");
  
  try {
    await docClient.put(params).promise();
    return { body: 'Successfully created item!' }
  } catch (err) {
    
    return { error: err }
  };
  


  ////////////////////////////////////  Case not use Cloudtrail
  
  // const snsMessage = JSON.parse(snsEvent['Message']);
  
  // const messageID = snsMessage['id'];  
  // const accountMessage = snsMessage['account'];
  // const sourceMessage = snsMessage['source'];
  // const detailTypeMessage = snsMessage['detail-type'];
  // const timeMessage = snsMessage['time'];
  // const regionMessage = snsMessage['region'];
  // const detailState = snsMessage['detail']['state'];

  // const messageData = [{
  //   messageID: `${messageID}`,
  //   accountMessage: `${accountMessage}`,
  //   sourceMessage: `${sourceMessage}`,
  //   detailTypeMessage: `${detailTypeMessage}`,
  //   detailState: `${detailState}`,
  //   regionMessage: `${regionMessage}`,
  //   timeMessage: `${timeMessage}`
  // }];
  
  // const messageCSV = jsonToCSV(messageData);
  // console.log(messageCSV);
  // fs.writeFileSync(`${messageID}.csv`, messageCSV);

  ////////////////////////////////////



  ///////////////////////////////////
  // s3 정보
  // const bucketName = `dev-pscard-lambda-slackalarm-bucket/${today.getFullYear()}/${today.getMonth()+1}/${today.getDate()}`;
  // putSla//ckEventToS3(bucketName, messageID, messageCSV);
  // console.log("json file upload to S3 Success");



  //////////////////////// processing data

  // if (sourceMessage.search("iam") >= 0){
  //   if (detailState.search("Detach") >= 0 || detailState.search("Delete") >= 0 || detailState.search("Revoke") >= 0 ){
  //     COLOR = RED;
  //   } else {
  //     COLOR = GREEN;
  //   }
  //   FALLBACK = "IAM 변경 감지";
    
  // } else if(sourceMessage.search("signin") >= 0){
  //   COLOR = BLUE;
  //   FALLBACK = "Console Login 감지";
  // } else if(sourceMessage.search("ec2") >= 0){
  //     if (detailState.search("Stop") >= 0 || detailState.search("Terminate") >= 0 || detailState.search("Revoke") >= 0 || detailState.search("stopped") >= 0 || detailState.search("stopping") >= 0){
  //       COLOR = RED;
  //     } else {
  //       COLOR = GREEN;
  //     }
  //     FALLBACK = "EC2 변경 감지";
  // } else if(sourceMessage.search("lambda") >= 0){
  //   if (detailState.search("Stop") >= 0 || detailState.search("Terminate") >= 0 || detailState.search("Revoke") >= 0 || detailState.search("stopped") >= 0 || detailState.search("stopping") >= 0){
  //     COLOR = RED;
  //   } else {
  //     COLOR = GREEN;
  //   }
  //   FALLBACK = "Lambda 변경 감지";
  // } else if(sourceMessage.search("dynamodb") >= 0){
  //   if (detailState.search("Delete") >= 0 ){
  //     COLOR = RED;
  //   } else if ( detailState.search("Describe") >= 0 || detailState.search("Update") >= 0 || detailState.search("List") >= 0){
  //     COLOR = GREEN;
  //   }
  //   FALLBACK = "DynamoDB 변경 감지";
  // } else{
  //   COLOR = MAGENTA;
  // }
    
    
  // let slackWebhookPayload = {
  //     "channel": CHANNEL,
  //     "attachments": [{
  //       "fallback": FALLBACK,
  //       "color": COLOR,
  //       "blocks": [
  //         {
  //         "type": "section",
  //         "fields": [
  //             {
  //               "type": "mrkdwn",
  //               "text": `${accountMessage}부터 발생!! `
  //             },
  //             {
  //               "type": "mrkdwn",
  //               "text": `소스 이름: ${sourceMessage}`
  //             },
  //             {
  //               "type": "mrkdwn",
  //               "text": `이벤트 타입: ${detailTypeMessage}`
  //             },
  //             {
  //               "type": "mrkdwn",
  //               "text": `이벤트 상태: ${detailState}`
  //             },
  //             {
  //               "type": "mrkdwn",
  //               "text": `리전: ${regionMessage}`
  //             },
  //             {
  //               "type": "mrkdwn",
  //               "text": `발생 시간: ${timeMessage}`
  //             }
              
  //           ]
  //         },
  //         {
  //           "type": "divider"
  //         }
  //       ]
  //     }]
  // }
    
 
  // let requestBody = slackWebhookPayload;
  // await postSlack(requestBody, SLACK_URL);
    


  // ddb.putItem(params, function(err, data) {
  //   if (err) {
  //     console.log("Error", err);
  //   } else {
  //     console.log("Success", data);
  //   }
  // });
  
///////////////////////////

//////////////////////////////

// ///////////////////////////////////////


//   ddb.describeTable(params, function(err, data) {
//     if (err) {
//       console.log("Error", err);
//     } else {
//       console.log("Describe Success", data.Table.KeySchema);
//     }
//   });
  
  
//   ///////// 항목 가져오기
//   var getParams = {
//     TableName: 'test-slackalarm-db',
//     Key: {'sourceMessage': 'aws.ec2'}
//   };
  
//   docClient.get(getParams, function(err, data) {
//     if (err) {
//       console.log("Get Error", err);
//     } else {
//       console.log("Get data Success", data.Item);
//     }
//   });
  
// ////////////////////////////////////////////

  // return 'Slack Alarm is sent';
}



const postSlack = async (message, slackUrl) => {
    return await Req(options(slackUrl), message);
};

const options = (slackUrl) => {
    const {host, pathname} = new URL(slackUrl);
    return {
        hostname: host,
        path: pathname,
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
    };
};

function payloadTemplate(userType, userName, awsRegion, eventTime, eventName, eventSource, sourceIP, cloudTrailUrl) {
  return {
    "channel": CHANNEL,
    "attachments": [{
      "fallback": FALLBACK,
      "color": COLOR,
      "blocks": [
        {
        "type": "section",
        "fields": [
            {
              "type": "mrkdwn",
              "text": `${userType} - (${userName}) 부터 발생!!  `
            },
            {
              "type": "mrkdwn",
              "text": `접속 리전: *${awsRegion}*, `
            },
            {
              "type": "mrkdwn",
              "text": `접속시간: ${eventTime}, `
            },
            {
              "type": "mrkdwn",
              "text": `이벤트 이름: ${eventName}`
            },
            {
              "type": "mrkdwn",
              "text": `이벤트 소스: ${eventSource}`
            },
            {
                "type": "mrkdwn",
                "text": `*소스 IP* :  ${sourceIP} :   (<https://ko.infobyip.com/ip-${sourceIP}.html|*IP 위치 조회*>)\n`
            }
          ]
        },
        {
          "type": "actions",
          "elements": [
              {
                "type": "button",
                "text": { 
                  "type": "plain_text",
                  "text": `CloudTrail 내용 확인::   ${EMOJI}`
                },
              "style": "primary",
              "url": cloudTrailUrl
              }
          ]
          
        },
        {
          "type": "divider"
        }]
    }]
  }
}

function Req(options, data) {

    return new Promise((resolve, reject) => {
        const req = https.request(options, (res) => {
            res.setEncoding('utf8');
            let responseBody = '';

            res.on('data', (chunk) => {
                responseBody += chunk;
            });

            res.on('end', () => {
                resolve(responseBody);
            });
        });

        req.on('error', (err) => {
            console.error(err);
            reject(err);
        });

        req.write(JSON.stringify(data));
        req.end();
    });
}


function dateFormat(date){
  return date >= 10 ? date : '0' + date; 
}
// const putSlackEventToS3 = async(bucket, id, data) => {
  
//   let params = {
//     Bucket: bucket,
//     Key : id + '.csv',
//     Body: data,
//   }

//   s3.putObject(params, function(err, data){
//     if(err) console.log(err, err.stack);
//     else    console.log(data);
//   });

//   const UPLOAD = await s3.upload(params).promise();
//   console.log("Successfully uploaded", UPLOAD);

//   return "Successfully uploaded";

// }


// json -> csv

// function jsonToCSV(json_data) {

//   // 1-1. json 데이터 취득
//   const json_array = json_data;
//   // 1-2. json데이터를 문자열(string)로 넣은 경우, JSON 배열 객체로 만들기 위해 아래 코드 사용
//   // const json_array = JSON.parse(json_data);
  

//   // 2. CSV 문자열 변수 선언: json을 csv로 변환한 문자열이 담길 변수
//   let csv_string = '';


//   // 3. 제목 추출: json_array의 첫번째 요소(객체)에서 제목(머릿글)으로 사용할 키값을 추출
//   const titles = Object.keys(json_array[0]);

  
//   // 4. CSV문자열에 제목 삽입: 각 제목은 컴마로 구분, 마지막 제목은 줄바꿈 추가
//   titles.forEach((title, index)=>{
//       csv_string += (index !== titles.length-1 ? `${title},` : `${title}\r\n`);
//   });


//   // 5. 내용 추출: json_array의 모든 요소를 순회하며 '내용' 추출
//   json_array.forEach((content, index)=>{
      
//       let row = ''; // 각 인덱스에 해당하는 '내용'을 담을 행

//       for(let title in content){ // for in 문은 객체의 키값만 추출하여 순회함.
//           // 행에 '내용' 할당: 각 내용 앞에 컴마를 삽입하여 구분, 첫번째 내용은 앞에 컴마X
//           row += (row === '' ? `${content[title]}` : `,${content[title]}`);
//       }

//       // CSV 문자열에 '내용' 행 삽입: 뒤에 줄바꿈(\r\n) 추가, 마지막 행은 줄바꿈X
//       csv_string += (index !== json_array.length-1 ? `${row}\r\n`: `${row}`);
//   })

//   // 6. CSV 문자열 반환: 최종 결과물(string)
//   return csv_string;
// }





