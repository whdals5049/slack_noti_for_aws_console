# modules/lambda/main.tf

# Create the IAM role for the Lambda function
resource "aws_iam_role" "lambda" {
  name = "${var.environment}-jmkim-slackAlarm-lambda-role"

  assume_role_policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Effect    = "Allow"
        Principal = {
          Service = "lambda.amazonaws.com"
        }
        Action = "sts:AssumeRole"
      }
    ]
  })
}

# Attach a policy to the IAM role that allows the Lambda function to read from the SNS topic
data "aws_iam_policy_document" "lambda" {
  statement {
    effect = "Allow"
    actions = [
      "sns:Get*",
      "sns:List*",
      "sns:Subscribe",
      "sns:Unsubscribe",
      "sns:Receive"
    ]
    resources = [
      var.sns_topic_arn      ######### 수정
    ]
  }
}

resource "aws_iam_role_policy_attachment" "lambda" {
  policy_arn = "arn:aws:iam::aws:policy/service-role/AWSLambdaBasicExecutionRole"
  role       = aws_iam_role.lambda.name
}

resource "aws_iam_role_policy_attachment" "sns" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonSNSFullAccess"
  role       = aws_iam_role.lambda.name
}

# resource "aws_iam_role_policy_attachment" "sns-read" {
#   policy_arn = aws_sns_topic_policy.sns.arn
#   role       = aws_iam_role.lambda.name
# }

# Create the Lambda function deployment package
# resource "archive_file" "lambda" {
#   type        = "zip"
#   source_dir  = "${path.module}/lambda"
#   output_path = "${path.module}/app.zip"
# }

# Create the Lambda function
resource "aws_lambda_function" "lambda" {
  function_name    = "${var.environment}-jmkim-slackAlarm-lambda-function"
  handler          = "app.handler"
  runtime          = "nodejs18.x"
  filename         = "../../../modules/lambda/app.zip"
  role             = aws_iam_role.lambda.arn

  environment {
    variables = {
      CHANNEL = "#devops",
      HOOK_URL = "https://hooks.slack.com/services/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"
    }
  }

}

# # Add environment variables to the Lambda function
# resource "aws_lambda_function_environment" "lambda" {
#   function_name = aws_lambda_function.lambda.function_name
#   variables = {
#     SNS_TOPIC_ARN = var.sns_topic_arn
#   }
# }

# Create a CloudWatch Logs group for the Lambda function
resource "aws_cloudwatch_log_group" "lambda" {
  name = "/aws/lambda/${aws_lambda_function.lambda.function_name}"
}

# Add a permission to the SNS topic to invoke the Lambda function
resource "aws_lambda_permission" "sns" {
  statement_id  = "${var.environment}-jmkim-slackAlarm-sns"
  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.lambda.function_name
  principal     = "sns.amazonaws.com"
  source_arn    = var.sns_topic_arn
}
