variable "environment" {
  type = string
  description = "deployment environment"
}

variable "sns_topic_arn" {
  description = "The ARN of the SNS topic to subscribe to"
  type        = string
}

